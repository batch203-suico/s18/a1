console.log('hello world');

// #1
function answerToOnepointOne(addOne, addTwo){
	let sumOfOne = addOne + addTwo;

	console.log('Displayed sum of '+addOne+' and '+addTwo);
	// console.log(sumOfOne);
	return(sumOfOne);
}

console.log(answerToOnepointOne(5,15));


// answerToOnepointOne(5,15);

function answerToOnepointTwo(diffOne, diffTwo){
	let diffofOne = diffOne - diffTwo;

	console.log('Displayed difference of '+diffOne+' and '+diffTwo);
	console.log(diffofOne);
}

answerToOnepointTwo(20,5);

// #2
function answerToTwoPointOne(addOne, addTwo){
	let prodOfTwo = addOne * addTwo;

	console.log('The product of '+addOne+' and '+addTwo+': ');
	// console.log(prodOfTwo);

	return(prodOfTwo);
}

console.log(answerToTwoPointOne(50,10));

function answerToTwoPointTwo(addOne, addTwo){
	let qoutientOfTwo = addOne / addTwo;

	console.log('The qoutient of '+addOne+' and '+addTwo+': ');
	// console.log(qoutientOfTwo);
	return(qoutientOfTwo);
}

console.log(answerToTwoPointTwo(50,10));


// #3

function getAreaOfCircle(radius){
		areaofCircle =Math.PI * radius**2;
		console.log('The result of getting the area of a circle with '+radius+'radius:')
		// console.log(areaofCircle);
		return(areaofCircle);
}

console.log(getAreaOfCircle(15));

// #4

function getAverageOfValue(aveOne, aveTwo, aveThree, aveFour){
		sumOfValues = aveOne+aveTwo+aveThree+aveFour;
		averageValue = sumOfValues/4;
		console.log('The average of '+aveOne+','+aveTwo+','+aveThree+' and '+aveFour+':');
		// console.log(averageValue);
		return(averageValue);
}

console.log(getAverageOfValue(20,40,60,80));

// #5

function getScoreData(gradeOne,gradeTwo){
		passingScore = 37.5;
		sumOfScore = gradeOne + gradeTwo;
		divideScore = sumOfScore/2;
		console.log('is '+divideScore+'/50 a passing score?')
		return(divideScore >= passingScore);
		
		// return(divideScore);
		// console.log(divideScore);
}

console.log(getScoreData(39,37));